# Bài 1
print("Bài 1:")
# Gọi thư viện toán
import math
# Hàm tính thể tích của 1 hình cầu
def print_volume (r) :
    v = 4/3*math.pi*r*3
    return v
#Xuất kết quả:
print("Kết quả r lần 1: ",print_volume(10))
print("Kết quả r lần 2: ",print_volume(20))
print("Kết quả r lần 3: ",print_volume(30))
# Bài 2
print("Bài 2: ")
#Hàm in

def xuatmanhinh (a) :
    return ("Chào bạn nhó",a)
print("kết quả lần 1: ",xuatmanhinh("Nguyên"))
print("kết quả lần 2: ",xuatmanhinh("Huynh"))
print("kết quả lần 3: ",xuatmanhinh("Nhân"))
# Mô tả :
""" 
 Xuất ra tên 
 input: nguyên 
 output: Chào bạn nhó nguyên
"""