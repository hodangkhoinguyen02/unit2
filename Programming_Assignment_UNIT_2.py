def new_line():# ra 1 dấu chấm
    print('.')
def three_lines():# ra 3 dấu chấm
    new_line()
    new_line()
    new_line()
print("9 dấu chấm")
def nine_lines (): # ra 9 dấu chấm
    three_lines()
    three_lines()
    three_lines()
nine_lines()
print("25 dấu chấm")
def clear_screen () : # Ra 25 dấu chấm
    nine_lines()
    nine_lines()
    three_lines()
    three_lines()
    new_line()
clear_screen()
""" 
Câu hỏi 1: Phép gán có triển khai các hàm new_line, three_lines, nine_lines và clear_screen 
cũng như phần chính của chương trình gọi các hàm không? 
Câu trả lời 1: Dạ có ạ 

Câu hỏi 2: Bài tập có chứng minh việc sử dụng các lệnh gọi hàm lồng nhau không? 
Câu trả lời 2: Dạ có ạ 


Câu hỏi 3: Việc gán có tạo ra kết quả thích hợp khi được thực thi không? 
Kết quả đầu ra phải được ghi lại trong tệp văn bản, tài liệu Microsoft Word hoặc tài 
liệu có định dạng RTF bằng cách sao chép kết quả đầu ra từ tập lệnh Python vào tài liệu. 
Ví dụ: tập lệnh thành công sẽ in ra 9 "." dòng đầu tiên và sau đó là 25 "." các dòng.
Câu trả lời 3: Dạ có ạ  

Câu hỏi 4: Mã chương trình có bao gồm các chú thích khi thích hợp không?
Câu trả lời 4: Này em chưa rõ ạ

"""




